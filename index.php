<?php get_header(); ?>


<div class="hero hero-content hero-single">
	<div class="wrapper">
    
    	<h2><?php echo apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) ); ?></h2>    
    
	</div><!--wrapper-->
</div><!--hero-->


<div class="content">
	<div class="wrapper">
    
	   
		<div class="news-container">      
       
       
       <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			 query_posts(array(
			   'posts_per_page' => 10,
			   'paged' => $paged
			  )
			 ); ?>
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        	<div class="news-item" id="post-<?php the_ID(); ?>">
                <?php if ( has_post_thumbnail() ) {
  				echo '<div class="news-thumb"><div>'; } ?>
                	<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
                <?php if ( has_post_thumbnail() ) {
  				echo '</div></div>'; } ?>
                <div class="news-summary">
                    <h4><?php the_title(); ?></h4>
					<p class="meta"><?php the_time(); ?></p>
					<p><?php excerpt('16'); ?></p>
                    <p class="news-link">Read more</p>
                </div>
                <a href="<?php the_permalink(); ?>"></a>
            </div>
			
			<?php endwhile; endif; ?>
    	        
        
        </div>        
        
        <aside>
        	<?php
                    if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog sidebar') ) :
            endif; ?>
        </aside>


	</div><!--wrapper-->


</div><!--content-->


<?php get_footer(); ?>