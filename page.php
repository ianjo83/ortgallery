<?php get_header(); ?>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero generic-hero">
	<div class="wrapper">
   
	
			<h2><?php the_title(); ?></h2>   

    
	</div><!--wrapper-->
</div><!--hero-->


<div class="content page-content generic-content">
	<div class="wrapper">


		<section>


    		<article class="two-col">

    			<?php the_content(); ?>

    		</article>


    	</section>	
    
   
	</div><!--wrapper-->
</div><!--content-->


<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>



<?php endwhile; ?>
<?php endif; ?>



<?php get_sidebar( 'gallery-full' ); ?>


<?php get_footer(); ?>