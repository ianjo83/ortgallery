<?php
/*
Template Name: Home
*/
get_header(); ?>




<div class="hero">
    <div class="wrapper">
        

        <div class="hero-container">
                

            <h2 class="hero-title">Currently at Ort Gallery</h2>


            <div class="hero-tabs">

                <?php
                $args = array(  'post_type' => 'exhibitions',
                                'meta_query' => array(
                                    array(
                                        'key'   => 'exhibition_status',
                                        'value' => 'current',
                                    )
                                ),
                                'meta_key'  => 'start_date',
                                'orderby'   => 'meta_value',
                                'order'     => 'ASC',
                                'numberposts' => 3,

                );
                $lastposts = get_posts( $args );
                foreach($lastposts as $post) : setup_postdata($post); ?>
        
             
                    <figure>
                        <a href="<?php the_permalink(); ?>">
                            <div class="hero-img two-col img-contain"><div>
                                <?php the_post_thumbnail('large'); ?>
                            </div></div>
                        </a>
                    </figure>


                <?php endforeach; ?> 


                <div class="hero-summary one-col hero-tabs-nav">


                    <?php
                    $args = array(  'post_type' => 'exhibitions',
                                    'meta_query' => array(
                                        array(
                                            'key'   => 'exhibition_status',
                                            'value' => 'current',
                                        )
                                    ),
                                    'meta_key'  => 'start_date',
                                    'orderby'   => 'meta_value',
                                    'order'     => 'ASC',
                                    'numberposts' => 3,

                    );
                    $lastposts = get_posts( $args );
                    foreach($lastposts as $post) : setup_postdata($post); ?>
                    
                        <div class="summary"><div>
                            
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                            <?php if( get_field('artist_name') ): ?>
                            <?php $field_name = "artist_name"; $field = get_field_object($field_name);
                            echo '<span class="meta credit">' . $field['value'] . '</span>';
                            ?><?php endif; ?>

                            <span class="date">
                                <?php if( get_field('start_date') ): ?>
                                <?php $field_name = "start_date"; $field = get_field_object($field_name);
                                echo $field['value'] . ' &#8212; ';
                                ?><?php endif; ?>

                                <?php if( get_field('end_date') ): ?>
                                <?php $field_name = "end_date"; $field = get_field_object($field_name);
                                echo $field['value'];
                                ?><?php endif; ?>
                            </span>
                            <div class="tab-overlay"></div>
                       
                        </div></div>

                    <?php endforeach; ?>  

                </div>

                            
            </div><!--hero-tabs-->

        </div>

    
    </div><!--wrapper-->
</div><!--hero-->








<div class="content coming-block">
    <div class="wrapper">

        <h2>Coming up at Ort Gallery</h2>


        <div class="two-col-grid coming-grid">


            <?php
                $args = array(  'post_type' => 'exhibitions',
                                'meta_query' => array(
                                    array(
                                        'key'   => 'exhibition_status',
                                        'value' => 'coming_up',
                                    )
                                ),
                                'meta_key'  => 'start_date',
                                'orderby'   => 'meta_value',
                                'order'     => 'ASC',
                                'numberposts' => 3,

                );
                $lastposts = get_posts( $args );
                foreach($lastposts as $post) : setup_postdata($post); ?>


                    <div class="card">
                        
                        <a href="<?php the_permalink(); ?>">
                            <div class="coming-thumb img-contain"><div>
                                <?php the_post_thumbnail('thumbnail'); ?>
                            </div></div>
                        </a>

                        <div class="summary">
                                    
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                            <?php if( get_field('artist_name') ): ?>
                            <?php $field_name = "artist_name"; $field = get_field_object($field_name);
                            echo '<span class="meta credit">' . $field['value'] . '</span>';
                            ?><?php endif; ?>

                            <span class="date">
                                <?php if( get_field('start_date') ): ?>
                                <?php $field_name = "start_date"; $field = get_field_object($field_name);
                                echo $field['value'] . ' &#8212; ';
                                ?><?php endif; ?>

                                <?php if( get_field('end_date') ): ?>
                                <?php $field_name = "end_date"; $field = get_field_object($field_name);
                                echo $field['value'];
                                ?><?php endif; ?>
                            </span>

                        </div>

                    </div>

                <?php endforeach; ?> 


        </div>

    </div><!--wrapper-->
</div><!--coming-block-->








<div class="content">
    <div class="wrapper">
    
        <div class="three-col-grid home-grid">
   
    

            <div class="card one-col">
                

                <?php 

                $today = date("Ymd");
                
                $loop = new WP_Query( array(
                        'post_type' => 'events',
                        'meta_key'  => 'start_date',
                        'orderby'   => 'meta_value',
                        'paged' => $paged,
                        'order'     => 'ASC',
                        'posts_per_page' => 1,
                        'meta_query' => array(
                            'relation' => 'OR',
                                            array(
                                            'key'     => 'start_date',
                                            'compare'   => '>=',
                                            'value'     => $today,
                                            'type'      => 'DATE'
                                            ),
                                            array(
                                            'key'     => 'end_date',
                                            'compare'   => '>=',
                                            'value'     => $today,
                                            'type'      => 'DATE'
                                            ),
                            ),
                    ) 
                );
                if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>

                <?php if ( has_post_thumbnail() ) { ?>
                
                    <a href="<?php the_permalink(); ?>">        
                        <?php the_post_thumbnail('thumbnail'); ?>
                    </a>
               
                <?php 
                }else{ 
                ?>

                    <a href="<?php the_permalink(); ?>">
                        <img src="http://ortgallery.co.uk/ortwp/wp-content/uploads/2017/11/Ort-Gallery-placeholder.png" alt="Ort Gallery">
                    </a>

                <?php
                } 
                ?> 

                    <div class="summary">
                        <h2>Events</h2>

                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                        <?php if( get_field('event_location') ): ?>
                        <?php $field_name = "event_location"; $field = get_field_object($field_name);
                        echo '<span class="meta credit">' . $field['value'] . '</span>';
                        ?><?php endif; ?>    
                        

                        <span class="date">
                            <? if (get_field('start_date')) : ?>
                                <?php if( get_field('end_date') ) { ?>

                                    <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                    echo $date->format('j F') . ' &#8212; '; ?>
        
                                    <? $date = DateTime::createFromFormat('Ymd', get_field('end_date'));
                                    echo $date->format('j F Y'); ?>

                                <?php }else{ ?>
                                
                                <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                    echo $date->format('j F Y'); ?>

                                <?php } ?> 

                            <?php endif; ?>

                            <?php if( get_field('event_time') ): ?>
                            <?php $field_name = "event_time"; $field = get_field_object($field_name);
                            echo '<br>' . $field['value'];
                            ?><?php endif; ?>

                        </span>

                        <a href="<?php echo home_url(); ?>/events" class="card-link">View all events</a>
                    </div>


                <?php endwhile; ?>

    
                <?php else:; ?>

                    <a href="<?php echo home_url(); ?>/events">
                    
                        <?php
                        $image_id = get_field('fallback_img', 165);
                        $image_size = 'thumbnail';
                        $image_array = wp_get_attachment_image_src($image_id, $image_size);
                        $event_image_url = $image_array[0];
                        ?>

                        <img src="<?php echo $event_image_url; ?>" alt="Events at Ort Gallery">
                    </a>

                    <div class="summary">
                        <h2>Events</h2>

                        <p><?php the_field('homepage_text', 165); ?></p>

                        <a href="<?php echo home_url(); ?>/events" class="card-link">More about our events programme</a>
                    </div>


                <?php endif; wp_reset_postdata(); ?>
            
            </div>

            



            <div class="card one-col">
                
                <?php 

                $today = date("Ymd");
                
                $loop = new WP_Query( array(
                        'post_type' => 'workshops',
                        'meta_key'  => 'start_date',
                        'orderby'   => 'meta_value',
                        'paged' => $paged,
                        'order'     => 'ASC',
                        'posts_per_page' => 1,
                        'meta_query' => array(
                            'relation' => 'OR',
                                            array(
                                            'key'     => 'start_date',
                                            'compare'   => '>=',
                                            'value'     => $today,
                                            'type'      => 'DATE'
                                            ),
                                            array(
                                            'key'     => 'end_date',
                                            'compare'   => '>=',
                                            'value'     => $today,
                                            'type'      => 'DATE'
                                            ),
                            ),
                    ) 
                );
                if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>

                
                <?php if ( has_post_thumbnail() ) { ?>
                
                    <a href="<?php the_permalink(); ?>">        
                        <?php the_post_thumbnail('thumbnail'); ?>
                    </a>
               
                <?php 
                }else{ 
                ?>

                    <a href="<?php the_permalink(); ?>">
                        <img src="http://ortgallery.co.uk/ortwp/wp-content/uploads/2017/11/Ort-Gallery-placeholder.png" alt="Ort Gallery">
                    </a>

                <?php
                } 
                ?> 

                    <div class="summary">
                        <h2>Workshops</h2>

                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                        <?php if( get_field('event_location') ): ?>
                        <?php $field_name = "event_location"; $field = get_field_object($field_name);
                        echo '<span class="meta credit">' . $field['value'] . '</span>';
                        ?><?php endif; ?>    
                        

                        <span class="date">
                            <?php if (get_field('start_date')) : ?>
                                <?php if( get_field('end_date') ) { ?>

                                    <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                    echo $date->format('j F') . ' &#8212; '; ?>
        
                                    <? $date = DateTime::createFromFormat('Ymd', get_field('end_date'));
                                    echo $date->format('j F Y'); ?>

                                <?php }else{ ?>
                                
                                <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                    echo $date->format('j F Y'); ?>

                                <?php } ?> 

                            <?php endif; ?>
                            
                            <?php if( get_field('event_time') ): ?>
                            <?php $field_name = "event_time"; $field = get_field_object($field_name);
                            echo '<br>' . $field['value'];
                            ?><?php endif; ?>
                        </span>

                        <a href="<?php echo home_url(); ?>/workshops" class="card-link">View all workshops</a>
                    </div>

    
                <?php endwhile; ?>

    
                <?php else:; ?>

                    <a href="<?php echo home_url(); ?>/workshops">
                    
                        <?php
                        $image_id = get_field('fallback_img', 53);
                        $image_size = 'thumbnail';
                        $image_array = wp_get_attachment_image_src($image_id, $image_size);
                        $workshop_image_url = $image_array[0];
                        ?>

                        <img src="<?php echo $workshop_image_url; ?>" alt="Workshops at Ort Gallery">
                    </a>

                    <div class="summary">
                        <h2>Workshops</h2>

                        <p><?php the_field('homepage_text', 53); ?></p>

                        <a href="<?php echo home_url(); ?>/workshops" class="card-link">More about our workshop programme</a>
                    </div>


                <?php endif; wp_reset_postdata(); ?>

            
            </div>

            
            <div class="card one-col">
                <?php $my_query = new WP_Query('page_id=166');
                while ($my_query->have_posts()) : $my_query->the_post();
                $do_not_duplicate = $post->ID;?>
 
                <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumbnail'); ?>
                </a>

                <div class="summary">
                    <h2>Membership</h2>
                    
                    <?php if( get_field('membership_home_summary') ): ?>
                    <?php $field_name = "membership_home_summary"; $field = get_field_object($field_name);
                    echo $field['value'];
                    ?><?php endif; ?>

                    <a href="<?php echo home_url(); ?>/schwarmerei" class="card-link">More about Schwarmerei</a>
                </div>

                <?php endwhile; ?>
            </div>


        


        </div><!--three-col-grid-->
    

    </div><!--wrapper-->
</div><!--content-->




<?php get_sidebar( 'gallery-full' ); ?>






<?php get_footer(); ?>