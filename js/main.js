// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.


// Responsive menu toggleClass

$(document).ready(function(){
    $(".device-nav").click(function(){
        $("nav").toggleClass("reveal");
    $(".device-nav").toggleClass("reveal");
    });
});

$(document).ready(function(){
  $('.device-nav').click(function(){
    $('.hamburger').toggleClass('is-active');
  });
});



// Configure Slick gallery


$(document).ready(function(){
  $('.hero-tabs').slick({
    slide: 'figure',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    dots: false,
    arrows: false,
    speed: 340,
    fade: true,
    cssEase: 'linear',
    adaptiveHeight: true,
    asNavFor: '.hero-tabs-nav'
  });
  $('.hero-tabs-nav').on({
    beforeChange: function (event, slick, current_slide_index, next_slide_index) {
      $('.hero-tabs-nav .slick-slide').addClass('slick-tab-single');
    }
  }).slick({
    slidesToShow: 3,
    asNavFor: '.hero-tabs',
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });
});




$(document).ready(function(){
  $('.gallery').slick({
    slide: 'figure',
    dots: false,
    infinite: true,
    speed: 340,
    fade: true,
    cssEase: 'linear',
    adaptiveHeight: true,
    lazyLoad: 'progressive',
    prevArrow: $('.prev-arrow'),
    nextArrow: $('.next-arrow')
  });
});


$($(".gallery figure").get()).each(function(i) {
    $(this).find(".dot-display .num1").text(++i);
});

$(document).ready(function(){ 
  var divCount = $(".gallery figure").size();
  $(".dot-display .num2").html(divCount);
});




// Find all iframes
var $iframes = $( "iframe" );
 
// Find &#x26; save the aspect ratio for all iframes
$iframes.each(function () {
  $( this ).data( "ratio", this.height / this.width )
    // Remove the hardcoded width &#x26; height attributes
    .removeAttr( "width" )
    .removeAttr( "height" );
});
 
// Resize the iframes when the window is resized
$( window ).resize( function () {
  $iframes.each( function() {
    // Get the parent container&#x27;s width
    var width = $( this ).parent().width();
    $( this ).width( width )
      .height( width * $( this ).data( "ratio" ) );
  });
// Resize to fix all iframes on page load.
}).resize();