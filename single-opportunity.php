<?php get_header(); ?>


<div class="hero">
    <div class="wrapper">
        
    
        <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
    
        <div class="hero-container">


        

            

            

            <div class="hero-summary full-col">

            

                <div class="summary">
                    <h3><?php the_title(); ?></h3>

                    
    
               
                </div>

            </div>
             

        </div>       
    
    
    </div><!--wrapper-->
</div><!--hero-->



<div class="content article-content">
    <div class="wrapper">


    	<article class="two-col">

    		<?php the_content(); ?>

    	</article>


    	<aside class="one-col">

        	    <?php if( get_field('how_to_apply') ): ?>
                <?php $field_name = "how_to_apply"; $field = get_field_object($field_name);
                echo '<div class="sidebar-block"><h4>' . $field['label'] . '</h4><p>' . $field['value'] .'</p></div>';
                ?><?php endif; ?> 


        </aside>


	</div><!--wrapper-->
</div><!--content-->


<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>


<?php endwhile; ?>
<?php endif; ?> 




<?php get_sidebar( 'gallery-full' ); ?>


<?php get_footer(); ?>