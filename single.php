<?php
if ( is_singular( 'exhibitions' ) ) {
	include 'single-exhibition.php';
} elseif ( is_singular( 'events' ) ) {
	include 'single-event.php';
} elseif ( is_singular( 'workshops' ) ) {
	include 'single-event.php';
} elseif ( is_singular( 'opportunities' ) ) {
	include 'single-opportunity.php';
} else {
	include 'single-generic.php';
}
?>