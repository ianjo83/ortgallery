<?php
/*
Template Name: Contact
*/
get_header(); ?>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero generic-hero">
    <div class="wrapper">
   
    
            <h2><?php the_title(); ?></h2>   

    
    </div><!--wrapper-->
</div><!--hero-->


<div class="content page-content generic-content">
    <div class="wrapper">


        <section>

            <article class="full-col contact-article">

                <div class="half-col">
                    <?php the_content(); ?>
                </div>


                <div class="half-col">
                    <div class="google-maps">
                        <?php if( get_field('google_map') ): ?>
                        <?php $field_name = "google_map"; $field = get_field_object($field_name);
                        echo '<iframe src="' . $field['value'] . '" width="600" height="450" frameborder="0" style="border:0"></iframe>';
                        ?><?php endif; ?>
                    </div>
                </div>

            </article>







        
        
    
   
    </div><!--wrapper-->
</div><!--content-->


<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>



<?php endwhile; ?>
<?php endif; ?>




<?php get_sidebar( 'gallery-full' ); ?>


<?php get_footer(); ?>