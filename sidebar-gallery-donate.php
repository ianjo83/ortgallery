<?php dynamic_sidebar( 'gallery-donate' ); ?>

<div class="content gallery-block">
    <div class="wrapper">

        <div class="one-col gallery-block-img">

            <img src="<?php the_field('gallery_image', 393); ?>" src="Donate to Ort Gallery">

        </div>


        <div class="two-col gallery-info gallery-info-donate">

            <?php the_field('gallery_description', 393); ?>

        </div>



    </div><!--wrapper-->
</div><!--gallery-block-->