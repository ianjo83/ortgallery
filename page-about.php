<?php
/*
Template Name: About
*/
get_header(); ?>


<div class="content secondary-nav">
    <div class="wrapper">
        <nav id="secondary-menu" role="navigation" class="secondary-menu">
                
			<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav', 'theme_location' => 'secondary-navigation' ) ); ?>
		</nav>
    </div>
</div>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero">
    <div class="wrapper">
        
       
        <div class="hero-container">


        <?php if ( has_post_thumbnail() ) { ?>

            
            <?php the_post_thumbnail('large', array('class' => 'half-col')); ?>

            <div class="hero-summary half-col">

            <?php 
            }else{ 
            ?>

            <div class="hero-summary full-col">

            <?php
            } 
            ?> 

                <div class="summary">
                    <h3><?php the_title(); ?></h3>
                </div>

            </div>
             

        </div>       
    
    
    </div><!--wrapper-->
</div><!--hero-->



<div class="content page-content article-content">
	<div class="wrapper">


		<section>


    		<article class="two-col">

    			<?php the_content(); ?>

    		</article>


    		<aside class="one-col about-sidebar">

                    <?php the_field('gallery_mailing_list', 144); ?>

    		</aside>

    	</section>	
    
   
	</div><!--wrapper-->
</div><!--content-->



<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>



<?php endwhile; ?>
<?php endif; ?>



<?php get_sidebar( 'gallery-full' ); ?>


<?php get_footer(); ?>