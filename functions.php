<?php

// Custom menu registration
function register_my_menus() {
  register_nav_menus(
    array(
      'main-navigation' => __( 'Main Navigation' )
    )
  );
  register_nav_menus(
    array(
      'secondary-navigation' => __( 'Secondary Navigation' )
    )
  );
  register_nav_menus(
    array(
      'gallery-info-navigation' => __( 'Gallery info block' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Show home page link on custom menu
function home_page_menu_args( $args ) {
$args['show_home'] = true;
return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );
 
// Enable post thumbnails
add_theme_support('post-thumbnails');

add_image_size( 'small-thumbnail', 150, 150 );

// Add shortcode funtionality to widgets
add_filter('widget_text', 'do_shortcode');
	
// Enable custom background
add_theme_support('custom-background');

// Define exerpt length
function custom_read_more() {
    return '&#x2026;';
}
function excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit, custom_read_more());
}

// Enable JQuery
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
}

// Create a function to enqueue our scripts
function add_my_scripts() {
  // Enqueue the modernizr script file and specify that it should be placed in the <head>
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2.min.js', array(), '2.6.2', false );
  wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '1.0', true );
  wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array(), '1.5.9', true );
}
// Run this function during the wp_enqueue_scripts action
add_action('wp_enqueue_scripts', 'add_my_scripts');

//enqueue external font awesome stylesheet
function enqueue_our_required_stylesheets(){
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'); 
}
add_action('wp_enqueue_scripts','enqueue_our_required_stylesheets');


/**
 * CodeWidget Class
 */
class CodeWidget extends WP_Widget {
    /** constructor */
    function CodeWidget() {
        parent::WP_Widget(false, $name = 'CodeWidget');
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        $content = $instance['content'];
        echo $content;
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['content'] = $new_instance['content'];
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
        $content = esc_attr($instance['content']);
    ?>
    
    <p>
    <label for="<?php echo $this->get_field_id('content'); ?>"><?php _e('Content:'); ?></label>
	</p>
	<textarea class="widefat" cols="20" rows="16" id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name('content'); ?>"><?php echo $content; ?></textarea>

    <?php
    }

} // class CodeWidget

// register CodeWidget widget
add_action('widgets_init', create_function('', 'return register_widget("CodeWidget");'));

// add category class to body tag
add_filter('body_class','add_category_to_single', 10, 2);
function add_category_to_single($classes, $class) {
  if (is_single() ) {
    global $post;
    foreach((get_the_category($post->ID)) as $category) {
      // add category slug to the $classes array
      $classes[] = $category->category_nicename;
    }
  }
  // return the $classes array
  return $classes;
}

// add custom post type
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'exhibitions',
    array(
      'labels' => array(
        'name' => __( 'Exhibitions' ),
        'singular_name' => __( 'Exhibition' )
      ),
	  'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats'
     ),
      'public' => true,
      'has_archive' => false,
    )
  );
  register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'Event' )
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats'
      ),
      'public' => true,
      'has_archive' => false,
    )
  );
  register_post_type( 'workshops',
    array(
      'labels' => array(
        'name' => __( 'Workshops' ),
        'singular_name' => __( 'Workshop' )
      ),
    'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail', 'post-formats'
      ),
      'public' => true,
      'has_archive' => false,
    )
  );
  register_post_type( 'opportunities',
    array(
      'labels' => array(
        'name' => __( 'Opportunities' ),
        'singular_name' => __( 'Opportunity' )
      ),
    'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail', 'post-formats'
      ),
      'public' => true,
      'has_archive' => false,
    )
  );
  register_post_type( 'content-blocks',
    array(
      'labels' => array(
        'name' => __( 'Content blocks' ),
        'singular_name' => __( 'Content block' )
      ),
    'supports' => array( 'title', 'custom-fields', 'post-formats'
      ),
      'public' => true,
      'has_archive' => false,
    )
  );
}


/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}



?>