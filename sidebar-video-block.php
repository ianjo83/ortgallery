<?php dynamic_sidebar( 'video-block' ); ?>

<div class="content video-content">
    <div class="wrapper">


        <section>
            <div class="video-container">
                
                <?php if( get_field('video_block') ): ?>
                    <?php the_field('video_block'); ?>
                
                    <figcaption>
                    	<?php if( get_field('video_credit') ): ?>
                            <?php $field_name = "video_credit"; $field = get_field_object($field_name);
                            echo '<p>' . $field['label'] . ' &#8212; ';
                            ?>
                            <?php if( get_field('video_credit_url') ): ?>
                            <?php $field_name = "video_credit_url"; $field = get_field_object($field_name);
                            echo '<a href="' . $field['value'] . '" target="_blank">';
                            ?><?php endif; ?>
                            <?php if( get_field('video_credit') ): ?>
                            <?php $field_name = "video_credit"; $field = get_field_object($field_name);
                            echo $field['value'];
                            ?><?php endif; ?>
                            <?php if( get_field('video_credit_url') ): ?>
                            <?php $field_name = "video_credit_url"; $field = get_field_object($field_name);
                            echo '</a>';
                            ?><?php endif; ?>
                        <?php else:; ?>

                            <div class="video-spacer"></div>

                        <?php endif; ?>
                <?php endif; ?>

                </figcaption>
        
            </div>
        </section>




    </div><!--wrapper-->
</div><!--video-content-->