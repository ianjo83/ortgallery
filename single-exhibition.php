<?php get_header(); ?>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero">
    <div class="wrapper">
        
    
    
        <div class="hero-container">
            

            <?php if ( has_post_thumbnail() ) { ?>

            
            <?php the_post_thumbnail('large', array('class' => 'two-col')); ?>

            <div class="hero-summary one-col">

            <?php 
            }else{ 
            ?>

            <div class="hero-summary full-col">

            <?php
            } 
            ?> 

                <div class="summary">
                    <h3><?php the_title(); ?></h3>

                    <?php if( get_field('artist_name') ): ?>
                    <?php $field_name = "artist_name"; $field = get_field_object($field_name);
                    echo '<span class="meta credit">' . $field['value'] . '</span>';
                    ?><?php endif; ?>

                    <span class="date">
                        <?php if( get_field('start_date') ): ?>
                        <?php $field_name = "start_date"; $field = get_field_object($field_name);
                        echo $field['value'] . ' &#8212; ';
                        ?><?php endif; ?>

                        <?php if( get_field('end_date') ): ?>
                        <?php $field_name = "end_date"; $field = get_field_object($field_name);
                        echo $field['value'];
                        ?><?php endif; ?>
                    </span>
               
                </div>

            </div>

        </div>       
    
    
    </div><!--wrapper-->
</div><!--hero-->



<div class="content article-content">
    <div class="wrapper">


    	<article class="two-col">

    		<?php the_content(); ?>

    	</article>


    	<aside class="one-col">

                <?php if( get_field('artist_website') ): ?>
                <?php $field_name = "artist_website"; $field = get_field_object($field_name);
                echo '<div class="sidebar-block"><h4 style="margin-bottom:0;">Artist website</h4><p style="margin:0 0 1.5em 0;"><a href="' . $field['value'] . '" target="_blank">' . $field['value'] . '</a></p></div>';
                ?><?php endif; ?>

        		<?php if( get_field('exhibition_info') ): ?>
                <?php $field_name = "exhibition_info"; $field = get_field_object($field_name);
                echo '<div class="sidebar-block">' . $field['value'] . '</div>';
                ?><?php endif; ?>

            <?php 

			$posts = get_field('associated_events');

			if( $posts ): ?>
                <div class="sidebar-block">
				<h4>Associated events</h4>

			    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			            
			            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                        <?php if( get_field('event_location') ): ?>
                        <?php $field_name = "event_location"; $field = get_field_object($field_name);
                        echo '<span class="meta credit">' . $field['value'] . '</span>';
                        ?><?php endif; ?>
                        
			            <span class="date">
                        <?php if (get_field('start_date')) : ?>
                            <?php if( get_field('end_date') ) { ?>

                                <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                echo $date->format('j F') . ' &#8212; '; ?>
    
                                <? $date = DateTime::createFromFormat('Ymd', get_field('end_date'));
                                echo $date->format('j F Y'); ?>

                            <?php }else{ ?>
                            
                            <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                echo $date->format('j F Y'); ?>

                            <?php } ?> 

                        <?php endif; ?>
                        
                        <?php if( get_field('event_time') ): ?>
                        <?php $field_name = "event_time"; $field = get_field_object($field_name);
                        echo '<br>' . $field['value'];
                        ?><?php endif; ?>
                    </span>
			    
			    <?php endforeach; ?>

			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                 </div>
			<?php endif; ?>
           
        </aside>


    </div><!--wrapper-->
</div><!--content-->



<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>



<?php endwhile; ?>
<?php endif; ?> 



<?php get_sidebar( 'gallery-full' ); ?>



<?php get_footer(); ?>