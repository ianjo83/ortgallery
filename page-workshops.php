<?php
/*
Template Name: Workshops
*/
get_header(); ?>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero generic-hero">
    <div class="wrapper">
   
    
            <h2><?php the_title(); ?></h2>   

    
    </div><!--wrapper-->
</div><!--hero-->


<div class="content page-content generic-content">
    <div class="wrapper">


        <section>

            <article class="two-col">

                <?php the_content(); ?>

            </article>


            <?php endwhile; ?>
            <?php endif; ?>
        
        </section>  

        <section>

            <div class="single-grid">


            <?php 

            $today = date("Ymd");
            
            $loop = new WP_Query( array(
                    'post_type' => 'workshops',
                    'meta_key'  => 'start_date',
                    'orderby'   => 'meta_value',
                    'paged' => $paged,
                    'order'     => 'ASC',
                    'posts_per_page' => 30,
                    'meta_query' => array(
                        'relation' => 'OR',
                                        array(
                                        'key'     => 'start_date',
                                        'compare'   => '>=',
                                        'value'     => $today,
                                        'type'      => 'DATE'
                                        ),
                                        array(
                                        'key'     => 'end_date',
                                        'compare'   => '>=',
                                        'value'     => $today,
                                        'type'      => 'DATE'
                                        ),
                        ),
                ) 
            );
            if ( $loop->have_posts() ) :
            while ( $loop->have_posts() ) : $loop->the_post(); ?>
  

                <div class="card two-col">


            <?php if ( has_post_thumbnail() ) { ?>
                    <a href="<?php the_permalink(); ?>">        
                        <?php the_post_thumbnail('thumbnail'); ?>
                    </a>
            <?php 
            }else{ 
            ?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="http://ortgallery.co.uk/ortwp/wp-content/uploads/2017/11/Ort-Gallery-placeholder.png" alt="Ort Gallery">
                    </a>
            <?php
            } 
            ?> 

                    <div class="summary">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        
                        <?php if( get_field('event_location') ): ?>
                        <?php $field_name = "event_location"; $field = get_field_object($field_name);
                        echo '<span class="meta credit">' . $field['value'] . '</span>';
                        ?><?php endif; ?>


                        <span class="date">
                            <?php if (get_field('start_date')) : ?>
                                <?php if( get_field('end_date') ) { ?>

                                    <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                    echo $date->format('j F') . ' &#8212; '; ?>
        
                                    <? $date = DateTime::createFromFormat('Ymd', get_field('end_date'));
                                    echo $date->format('j F Y'); ?>

                                <?php }else{ ?>
                                
                                <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                    echo $date->format('j F Y'); ?>

                                <?php } ?> 

                            <?php endif; ?>
                            
                            <?php if( get_field('event_time') ): ?>
                            <?php $field_name = "event_time"; $field = get_field_object($field_name);
                            echo '<br>' . $field['value'];
                            ?><?php endif; ?>
                        </span>
                        
                    </div>
              
                </div>


            <?php endwhile; ?>

    
                <?php else:; ?>


                    <p><?php the_field('no_listing_text', 53); ?></p>


            <?php endif; wp_reset_postdata(); ?>

        
        </section>
    
   
    </div><!--wrapper-->
</div><!--content-->





<?php get_sidebar( 'gallery-donate' ); ?>


<?php get_footer(); ?>