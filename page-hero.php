<?php
/*
Template Name: Hero Image
*/
get_header(); ?>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero">
    <div class="wrapper">
        
    
        
    
        <div class="hero-container">


        <?php if ( has_post_thumbnail() ) { ?>

            
            <?php the_post_thumbnail('large', array('class' => 'half-col')); ?>

            <div class="hero-summary half-col">

            <?php 
            }else{ 
            ?>

            <div class="hero-summary full-col">

            <?php
            } 
            ?> 

                <div class="summary">
                    <h3><?php the_title(); ?></h3>
                </div>

            </div>
             

        </div>       
    
    
    </div><!--wrapper-->
</div><!--hero-->


<div class="content page-content generic-content">
	<div class="wrapper">


		<section>


    		<article class="two-col">

    			<?php the_content(); ?>

    		</article>


    	</section>	
    
   
	</div><!--wrapper-->
</div><!--content-->


<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>


<?php endwhile; ?>
<?php endif; ?>



<?php get_sidebar( 'gallery-full' ); ?>


<?php get_footer(); ?>