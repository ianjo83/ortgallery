<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js"><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?php wp_title ( '|', true,'right' ); ?></title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/boilerplate.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick.css" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" media="screen, projection" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,700,700i" rel="stylesheet">

<link rel="apple-touch-icon" sizes="180x180" href="http://ortgallery.co.uk/ortwp/wp-content/themes/ortgallery/images/favicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="http://ortgallery.co.uk/ortwp/wp-content/themes/ortgallery/images/favicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="http://ortgallery.co.uk/ortwp/wp-content/themes/ortgallery/images/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="http://ortgallery.co.uk/ortwp/wp-content/themes/ortgallery/images/favicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="http://ortgallery.co.uk/ortwp/wp-content/themes/ortgallery/images/favicons/favicon-16x16.png">

<?php wp_enqueue_script('jquery'); ?>


<?php wp_head(); ?>

</head>

<body <?php body_class(''); ?>>     

<!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<div id="container">


<header>

	<div class="wrapper">

    	<h1><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url')?>/images/ort-logo.png" alt="Ort Gallery" class="logo"></a></h1>
        
        <div class="device-nav" id="nav-icon">
            <div class="inner">
                <div class="text">Menu</div>
            	<button class="hamburger hamburger--minus" type="button">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </button>
            </div>
        </div>
        
             
        <nav id="menu" role="navigation" class="main-menu">

            <div class="sm-icons">
                <ul>
                    <li>
                        <a title="Ort Gallery's profile on Facebook" href="<?php the_field('facebook', 661); ?>" class="sm-icon sm-icon-facebook" target="_blank" rel="nofollow"></a>
                    </li>
                    <li>
                        <a title="Ort Gallery's profile on Twitter" href="<?php the_field('twitter', 661); ?>" class="sm-icon sm-icon-twitter" target="_blank" rel="nofollow"></a>
                    </li>
                    <li>
                        <a title="Ort Gallery's profile on Instagram" href="<?php the_field('instagram', 661); ?>" class="sm-icon sm-icon-instagram" target="_blank" rel="nofollow"></a>
                    </li>
                </ul>
            </div>
                
			<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav', 'theme_location' => 'main-navigation' ) ); ?>
		</nav>
        
        

	</div><!--wrapper-->
	
    
</header>