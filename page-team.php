<?php
/*
Template Name: Team
*/
get_header(); ?>


<div class="content secondary-nav">
    <div class="wrapper">
        <nav id="secondary-menu" role="navigation" class="secondary-menu">
                
			<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav', 'theme_location' => 'secondary-navigation' ) ); ?>
		</nav>
    </div>
</div>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero generic-hero">
	<div class="wrapper">
   
	
			<h2><?php the_title(); ?></h2>   

    
	</div><!--wrapper-->
</div><!--hero-->


<div class="content page-content generic-content">
	<div class="wrapper">


		<section>


    		<article class="two-col">

    			<?php the_content(); ?>

    		</article>

    		
    	</section>



    	<section>

    		<article>

    			<?php if( have_rows('team_member') ): ?>

					<div class="team-grid">

					<?php while( have_rows('team_member') ): the_row(); 

						// vars
						$name = get_sub_field('name');
						$role = get_sub_field('role');
						$description = get_sub_field('description');
						$email = get_sub_field('email');
						$link = get_sub_field('link');
						$image = get_sub_field('image');

						?>

						<div class="card">

							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
							
							<div class="text">

								<p><strong>

								<?php if( $link ): ?><a href="<?php echo $link; ?>" target="_blank"><?php endif; ?>

								<?php echo $name; ?><?php if( $link ): ?></a><?php endif; ?>

								</strong><br>

								<?php echo $role; ?></p>

								<p><?php echo $description; ?></p>
							
								<p><?php if( $email ): ?>

								<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>

								<?php endif; ?></p>

						    </div>

						</div>

					<?php endwhile; ?>

					</div>

				<?php endif; ?>

			</article>

		</section>
    
   
	</div><!--wrapper-->
</div><!--content-->


<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>


<?php endwhile; ?>
<?php endif; ?>



<?php get_sidebar( 'gallery-full' ); ?>


<?php get_footer(); ?>