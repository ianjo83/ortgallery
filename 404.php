<?php get_header(); ?>


<div class="hero generic-hero">
	<div class="wrapper">
   
	
			<h2>Page not found</h2>   

    
	</div><!--wrapper-->
</div><!--hero-->


<div class="content page-content generic-content">
	<div class="wrapper">


		<section>


    		<article class="two-col">

    			<p>The page you are looking for does not exist.</p>

    			<p>If you think something is wrong please <a href="<?php echo home_url(); ?>/contact">let us know</a>.

    		</article>


    	</section>	
    
   
	</div><!--wrapper-->
</div><!--content-->




<?php get_sidebar( 'gallery-full' ); ?>



<?php get_footer(); ?>