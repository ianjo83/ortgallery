<?php get_header(); ?>


<div class="hero">
    <div class="wrapper">
        
    
        <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
    
        <div class="hero-container">


        <?php if ( has_post_thumbnail() ) { ?>

            
            <?php the_post_thumbnail('large', array('class' => 'half-col')); ?>

            <?php 
            }else{ 
            ?>

            <img src="http://ortgallery.co.uk/ortwp/wp-content/uploads/2017/11/Ort-Gallery-placeholder.png" alt="Ort Gallery" class="half-col">
            
            <?php
            } 
            ?>

            <div class="hero-summary half-col">
             

                <div class="summary">
                    <h3><?php the_title(); ?></h3>

                    <?php if( get_field('event_location') ): ?>
                    <?php $field_name = "event_location"; $field = get_field_object($field_name);
                    echo '<span class="meta credit">' . $field['value'] . '</span>';
                    ?><?php endif; ?>

                    <span class="date">
                        <?php if (get_field('start_date')) : ?>
                            <?php if( get_field('end_date') ) { ?>

                                <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                echo $date->format('j F') . ' &#8212; '; ?>
    
                                <? $date = DateTime::createFromFormat('Ymd', get_field('end_date'));
                                echo $date->format('j F Y'); ?>

                            <?php }else{ ?>
                            
                            <? $date = DateTime::createFromFormat('Ymd', get_field('start_date'));
                                echo $date->format('j F Y'); ?>

                            <?php } ?> 

                        <?php endif; ?>
                        
                        <?php if( get_field('event_time') ): ?>
                        <?php $field_name = "event_time"; $field = get_field_object($field_name);
                        echo '<br>' . $field['value'];
                        ?><?php endif; ?>
                    </span>
               
                </div>

            </div>
             

        </div>       
    
    
    </div><!--wrapper-->
</div><!--hero-->



<div class="content article-content event-content">
    <div class="wrapper">


    	<article class="two-col">

    		<?php the_content(); ?>

    	</article>


    	<aside class="one-col">

        	    <?php if( get_field('event_location_full') ): ?>
                <?php $field_name = "event_location_full"; $field = get_field_object($field_name);
                echo '<div class="sidebar-block"><strong>' . $field['label'] . '</strong><br>' . $field['value'] .'</div>';
                ?><?php endif; ?> 

                <?php if( get_field('event_duration') ): ?>
                <?php $field_name = "event_duration"; $field = get_field_object($field_name);
                echo '<div class="sidebar-block"><strong>' . $field['label'] . '</strong><br>' . $field['value'] .'</div>';
                ?><?php endif; ?> 

                <?php if( get_field('event_price') ): ?>
                <?php $field_name = "event_price"; $field = get_field_object($field_name);
                echo '<div class="sidebar-block"><strong>' . $field['label'] . '</strong><br>' . $field['value'] .'</div>';
                ?><?php endif; ?> 

                <?php if( get_field('event_tickets') ): ?>
                <?php $field_name = "event_tickets"; $field = get_field_object($field_name);
                echo '<a href="' . $field['value'] . '" target="_blank">Get tickets here</a>';
                ?><?php endif; ?>            
        </aside>


	</div><!--wrapper-->
</div><!--content-->



<?php get_sidebar( 'video-block' ); ?>


<?php get_sidebar( 'image-carousel' ); ?>



<?php endwhile; ?>
<?php endif; ?> 



<?php get_sidebar( 'gallery-donate' ); ?>


<?php get_footer(); ?>