<?php
/*
Template Name: Opportunities
*/
get_header(); ?>


<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>


<div class="hero generic-hero">
    <div class="wrapper">
   
    
            <h2><?php the_title(); ?></h2>   

    
    </div><!--wrapper-->
</div><!--hero-->


<div class="content page-content generic-content">
    <div class="wrapper">


        <section>

            <article class="two-col">

                <?php the_content(); ?>

            </article>


            <?php endwhile; ?>
            <?php endif; ?>
        
        </section>  

        <section>

            <div class="two-col-grid">



            <?php

            $args = array(  'post_type' => 'opportunities',
                            'order'     => 'ASC',
                            'numberposts' => 22,
            );
            
            $lastposts = get_posts( $args );
            foreach($lastposts as $post) : setup_postdata($post); ?>

            
    


            <div class="card half-col">

                
                <div class="summary">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    
                    <?php if( get_field('opportunity_summary') ): ?>
                    <?php $field_name = "opportunity_summary"; $field = get_field_object($field_name);
                    echo '<p>' . $field['value'] . '</p>';
                    ?><?php endif; ?>

                    <p><a href="<?php the_permalink(); ?>">View opportunity</a></p>

        
                </div>
              
            </div>




             <?php endforeach; ?>



        </section>
    
   
    </div><!--wrapper-->
</div><!--content-->





<?php get_sidebar( 'gallery-full' ); ?>


<?php get_footer(); ?>