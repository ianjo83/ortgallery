<?php
/*
Template Name: Exhibitions
*/
get_header(); ?>


<div class="hero">
    <div class="wrapper">
        

        <div class="hero-container">
                

            <h2 class="hero-title">Currently at Ort Gallery</h2>


            <div class="hero-tabs">

                <?php
                $args = array(  'post_type' => 'exhibitions',
                                'meta_query' => array(
                                    array(
                                        'key'   => 'exhibition_status',
                                        'value' => 'current',
                                    )
                                ),
                                'meta_key'  => 'start_date',
                                'orderby'   => 'meta_value',
                                'order'     => 'ASC',
                                'numberposts' => 3,

                );
                $lastposts = get_posts( $args );
                foreach($lastposts as $post) : setup_postdata($post); ?>
        
             
                    <figure>
                        <a href="<?php the_permalink(); ?>">
                            <div class="hero-img two-col img-contain"><div>
                                <?php the_post_thumbnail('large'); ?>
                            </div></div>
                        </a>
                    </figure>


                <?php endforeach; ?> 


                <div class="hero-summary one-col hero-tabs-nav">


                    <?php
                    $args = array(  'post_type' => 'exhibitions',
                                    'meta_query' => array(
                                        array(
                                            'key'   => 'exhibition_status',
                                            'value' => 'current',
                                        )
                                    ),
                                    'meta_key'  => 'start_date',
                                    'orderby'   => 'meta_value',
                                    'order'     => 'ASC',
                                    'numberposts' => 3,

                    );
                    $lastposts = get_posts( $args );
                    foreach($lastposts as $post) : setup_postdata($post); ?>
                    
                        <div class="summary"><div>
                            
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                            <?php if( get_field('artist_name') ): ?>
                            <?php $field_name = "artist_name"; $field = get_field_object($field_name);
                            echo '<span class="meta credit">' . $field['value'] . '</span>';
                            ?><?php endif; ?>

                            <span class="date">
                                <?php if( get_field('start_date') ): ?>
                                <?php $field_name = "start_date"; $field = get_field_object($field_name);
                                echo $field['value'] . ' &#8212; ';
                                ?><?php endif; ?>

                                <?php if( get_field('end_date') ): ?>
                                <?php $field_name = "end_date"; $field = get_field_object($field_name);
                                echo $field['value'];
                                ?><?php endif; ?>
                            </span>
                            <div class="tab-overlay"></div>
                       
                        </div></div>

                    <?php endforeach; ?>  

                </div>

                            
            </div><!--hero-tabs-->

        </div>

    
    </div><!--wrapper-->
</div><!--hero-->




<div class="content coming-block">
    <div class="wrapper">

        <h2>Coming up at Ort Gallery</h2>


        <div class="two-col-grid coming-grid">


            <?php
                $args = array(  'post_type' => 'exhibitions',
                                'meta_query' => array(
                                    array(
                                        'key'   => 'exhibition_status',
                                        'value' => 'coming_up',
                                    )
                                ),
                                'meta_key'  => 'start_date',
                                'orderby'   => 'meta_value',
                                'order'     => 'ASC',
                                'numberposts' => 3,

                );
                $lastposts = get_posts( $args );
                foreach($lastposts as $post) : setup_postdata($post); ?>


                    <div class="card">
                        
                        <a href="<?php the_permalink(); ?>">
                            <div class="coming-thumb img-contain"><div>
                                <?php the_post_thumbnail('thumbnail'); ?>
                            </div></div>
                        </a>

                        <div class="summary">
                                    
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                            <?php if( get_field('artist_name') ): ?>
                            <?php $field_name = "artist_name"; $field = get_field_object($field_name);
                            echo '<span class="meta credit">' . $field['value'] . '</span>';
                            ?><?php endif; ?>

                            <span class="date">
                                <?php if( get_field('start_date') ): ?>
                                <?php $field_name = "start_date"; $field = get_field_object($field_name);
                                echo $field['value'] . ' &#8212; ';
                                ?><?php endif; ?>

                                <?php if( get_field('end_date') ): ?>
                                <?php $field_name = "end_date"; $field = get_field_object($field_name);
                                echo $field['value'];
                                ?><?php endif; ?>
                            </span>

                        </div>

                    </div>

                <?php endforeach; ?> 
                

        </div>

    </div><!--wrapper-->
</div><!--coming-block-->





<div class="content">
    <div class="wrapper">

    	<h2 class="header-pad">Exhibition archive</h2>
    
        <div class="three-col-grid">


        <?php
        $args = array(  'post_type' => 'exhibitions',
                        'meta_query' => array(
                            array(
                                'key'   => 'exhibition_status',
                                'value' => 'archive',
                            )
                        ),
                        'meta_key'  => 'end_date',
                        'orderby'   => 'meta_value',
                        'order'     => 'DESC',
                        'numberposts' => 22,

        );
        $lastposts = get_posts( $args );
        foreach($lastposts as $post) : setup_postdata($post); ?>


            

            <div class="card one-col">
                
                <a href="<?php the_permalink(); ?>">
                	<?php the_post_thumbnail('thumbnail'); ?>	
                </a>

                <div class="summary">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    
                    <?php if( get_field('artist_name') ): ?>
                    <?php $field_name = "artist_name"; $field = get_field_object($field_name);
                    echo '<span class="meta credit">' . $field['value'] . '</span>';
                    ?><?php endif; ?>

                    <span class="date">
                        <?php if( get_field('start_date') ): ?>
                        <?php $field_name = "start_date"; $field = get_field_object($field_name);
                        echo $field['value'] . ' &#8212; ';
                        ?><?php endif; ?>

                        <?php if( get_field('end_date') ): ?>
                        <?php $field_name = "end_date"; $field = get_field_object($field_name);
                        echo $field['value'];
                        ?><?php endif; ?>
                    </span>
                </div>
            </div>





        <?php endforeach; ?> 


        </div><!--three-col-grid-->
    

    </div><!--wrapper-->
</div><!--content-->


<?php get_sidebar( 'gallery-full' ); ?>

<?php get_footer(); ?>