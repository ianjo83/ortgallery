
<div class="content upper-footer">
    <div class="wrapper">

        <div class="three-col-grid footer-grid">
        

            <div class="card one-col">
                <div class="summary">
                    <?php the_field('gallery_address', 144); ?>
                </div>
            </div>

            
            <div class="card one-col">
                <div class="summary">
                    <?php the_field('gallery_opening_hours', 144); ?>
                </div>
            </div>

            
            <div class="card one-col">
                <div class="summary">
                    <?php the_field('gallery_mailing_list', 144); ?>
                </div>
            </div>
            

        </div><!--three-col-grid-->


    </div><!--wrapper-->
</div><!--content-->


<footer>
    <div class="wrapper">

        <div class="footer-left">
            <?php the_field('sub_footer_content', 666); ?>
        </div>


        <div class="footer-right">
            <p><?php the_field('website_credit', 666); ?></p>
        </div>
        


    </div><!--wrapper-->
</footer>



</div><!--container-->

  

<?php wp_footer(); ?>

</body>
</html>