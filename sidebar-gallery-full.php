<?php dynamic_sidebar( 'gallery-full' ); ?>

<div class="content gallery-block">
    <div class="wrapper">

        <div class="one-col gallery-block-img">

            <img src="<?php the_field('gallery_image', 153); ?>" alt="Ort Gallery">

        </div>


        <div class="two-col gallery-info">

            <?php the_field('gallery_description', 153); ?>

            
            <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'nav', 'theme_location' => 'gallery-info-navigation' ) ); ?>

        </div>



    </div><!--wrapper-->
</div><!--gallery-block-->