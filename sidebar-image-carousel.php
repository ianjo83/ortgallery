<?php dynamic_sidebar( 'image-carousel' ); ?>

<div class="content gallery-content">
    <div class="wrapper">


        <?php 

                $images = get_field('image_gallery');

                if( $images ): ?>

                    <section class="gallery">

                        <?php foreach( $images as $image ): ?>

                            <figure>
                                <div class="gallery-img-container">
                                    <img src="<?php bloginfo('template_url')?>/images/3x2.png">
                                    <img src="<?php echo $image['sizes']['large']; ?>" class="gallery-img" alt="<?php echo $image['title']; ?>" title="<?php echo $image['title']; ?>" />
                                </div>
                                <div class="gallery-meta">
                                    <div class="dot-display">
                                        <span class="num1"></span> / <span class="num2"></span>
                                    </div>
                                    <div class="gallery-nav">
                                        <div class="prev-arrow"></div>
                                        <div class="next-arrow"></div>
                                    </div>
                                    <figcaption>
                                        
	                                    <?php if( $image['description'] ): ?>
										<p><?php echo $image['description']; ?></p>
										<?php endif; ?>
                                        <?php if( get_field('image_credit') ): ?>
                                        <?php $field_name = "image_credit"; $field = get_field_object($field_name);
                                        echo '<p>' . $field['label'] . ' &#8212; ';
                                        ?><?php endif; ?>
                                        <?php if( get_field('photographer_website') ): ?>
                                        <?php $field_name = "photographer_website"; $field = get_field_object($field_name);
                                        echo '<a href="' . $field['value'] . '" target="_blank">';
                                        ?><?php endif; ?>
                                        <?php if( get_field('image_credit') ): ?>
                                        <?php $field_name = "image_credit"; $field = get_field_object($field_name);
                                        echo $field['value'];
                                        ?><?php endif; ?>
                                        <?php if( get_field('photographer_website') ): ?>
                                        <?php $field_name = "photographer_website"; $field = get_field_object($field_name);
                                        echo '</a>';
                                        ?><?php endif; ?>
                                       	</p>
                                    </figcaption>
                                </div>
                            </figure>
                                
                        <?php endforeach; ?>
        
                    </section>

                <?php endif; ?>


    </div><!--wrapper-->
</div><!--gallery-content-->